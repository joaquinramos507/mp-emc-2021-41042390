# EMC: 2021
# Nombre del Alumno: Joaquin Alexis Ramos
# DNI: 41042390
import os

def menu():
    os.system('cls')
    print('1) agregar un empleado')
    print('2) mostrar ID mayor o igual al ingresado')
    print('3) cambiar el departamento de un empleado ')
    print('4) ordenar los empleados en forma descendente')
    print('5) Fin')
    opcion = int(input('Ingrese una opción: '))
    while not(opcion >= 1 and opcion <= 5): 
        opcion = int(input('Ingrese una opción: '))
    return opcion    
def pulsarTecla():
    input('Pulse un tecla...')            
def cargarempleados():
    lista= [[100, "Steven King", "king@gmail.com", "Gerencia", 24000],
             [101, "Neena Kochhar", "neenaKochhar@gmail.com", "Ventas", 17000],
             [102, "Lex De Haan", "lexDeHaan@gmail.com", "Compras", 16000],
             [103, "Alexander Hunold", "alexanderHunold@gmail.com", "Compras", 9000],
             [104, "David Austin", "davidAustin@gmail.com", "Compras", 4800],
             [105, "Valli Pataballa", "valliPataballa@gmail.com", "Ventas", 4200],
             [106, "Nancy Greenberg", "nancyGreenberg@gmail.com", "Ventas", 5500],
             [107, "Daniel Faviet", "danielFaviet@gmail.com", "Ventas", 3000],
             [108, "John Chen", "johnChen@gmail.com", "Compras", 8200],
             [109, "Ismael Sciarra", "ismaelSciarra@gmail.com", "Compras", 7700],
             [110, "Alexander Khoo", "alexanderKhoo@gmail.com", "Comrpas", 3100]]
    respuesta = 's'
    while respuesta == 's':
        Id =int(input('ingrese su id: '))
        while Id in lista[0]:
            Id=int(input('ya existe, ingrese su id: '))
        nombre = input('Nombre: ')
        email = input('ingrese su email: ')
        departamento = elejirdepartamento()
        salario= float(input('ingrese su salario'))
        lista.append([Id, nombre, email, departamento, salario])
        respuesta = input('Continuar?: ')
    return lista
def idmayoroigual(empleados,id):
    for elem in empleados:
        if id<=elem[0]:
           print(elem[0],elem[1],elem[2],elem[3],elem[4])
def modificardepartamento(lista):
    id = int(input('ingrese id del empleado: '))
    pos=-1
    for i ,item in enumerate(lista):
        if item[0] == id:  
            pos = i
            if pos==-1:
               print('no existe ese id en nuestros registros')
            else:
               item[3]= input('ingrese cambio de departamento: (1=Gerencia, 2=Ventas, 3=Compras ')
               print(lista[pos])
def elejirdepartamento():
    ambito=0 
    nro=int(input('ingrese su departamento:(1=Gerencia, 2=Ventas, 3=Compras'))
    if nro==1:
        ambito='Gerencia'
    elif nro==2:
        ambito='Ventas'
    elif nro==3:
        ambito='Compras'
    return ambito
def ordenardescendente(list):
 n = len(list)
 for i in range(n):
    for j in range(0, n-1):
        if list[j] > list[j+1]:
           list[j], list[j+1] = list[j+1], list[j]
 return list
#Principal
opcion = 0
empleados = [[100, "Steven King", "king@gmail.com", "Gerencia", 24000],
             [101, "Neena Kochhar", "neenaKochhar@gmail.com", "Ventas", 17000],
             [102, "Lex De Haan", "lexDeHaan@gmail.com", "Compras", 16000],
             [103, "Alexander Hunold", "alexanderHunold@gmail.com", "Compras", 9000],
             [104, "David Austin", "davidAustin@gmail.com", "Compras", 4800],
             [105, "Valli Pataballa", "valliPataballa@gmail.com", "Ventas", 4200],
             [106, "Nancy Greenberg", "nancyGreenberg@gmail.com", "Ventas", 5500],
             [107, "Daniel Faviet", "danielFaviet@gmail.com", "Ventas", 3000],
             [108, "John Chen", "johnChen@gmail.com", "Compras", 8200],
             [109, "Ismael Sciarra", "ismaelSciarra@gmail.com", "Compras", 7700],
             [110, "Alexander Khoo", "alexanderKhoo@gmail.com", "Comrpas", 3100]]
while opcion != 5: 
    opcion = menu()
    if opcion == 1:
        empleados = cargarempleados()
    elif opcion == 2:
        id=int(input('ingrese valor'))
        print('personas mayor o igual al numero dado',idmayoroigual(empleados,id))
        pulsarTecla()
    elif opcion == 3:
        print(modificardepartamento(empleados))  
        pulsarTecla()   
    elif opcion == 4:
        print(ordenardescendente(empleados))      
        pulsarTecla()  
    elif opcion == 5:
        print('Fin del programa...')    
        pulsarTecla()
   
        